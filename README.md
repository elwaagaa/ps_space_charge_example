# PS Space Charge Example

Model of the Proton Synchrotron (PS) accelerator adapted for [Xsuite](https://xsuite.readthedocs.io/en/latest/) with space charge, in particular for ions. 

Objective is to create similar model to the [Super Proton Synchrotron (SPS) space charge example](https://gitlab.cern.ch/elwaagaa/sps_ion_space_charge_example).

The folder `First_footprint_and_dispersive_correction_tests` contains a script to test different methods to extract a tune footprint from tracking of particles in Xsuite, comparing with and without correction for the dispersion component. The obtain the betatronic matrices, the [statisticalEmittance](https://github.com/fasvesta/statistical_emittance/tree/master) repository is used. 
