"""
FIRST PS PB ION FOOTPRINT

Investigate Space Charge footprint for Pb54+ in the PS, compare with analytical  
- also compare different ways of correcting for dispersion

by Elias Waagaard
"""
import json

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
import matplotlib
import NAFFlib
import sys

from scipy.fft import fft, fftfreq

import xpart as xp
import xobjects as xo
import xtrack as xt
import xfields as xf

# Flags and settings
num_turns = 200
num_particles = 5000

# Load SPS ion sequence 
fname_line = '../PS_sequence/PS_2022_Pb_ions_matched_with_RF.json'
#fname_line = 'PS_sequence/PS_2022_Pb_ions_for_tracking.json'

sys.path.append('..')
from statisticalEmittance.statisticalEmittance import statisticalEmittance 
from PySCRDT.PySCRDT import PySCRDT
import resonance_lines
import acc_lib

#%% Set up the X-suite contexts and load sequence
with open(fname_line, 'r') as fid:
     input_data = json.load(fid)    
line = xt.Line.from_dict(input_data)

#%% Define different particles to track 
particle_ref = line.particle_ref
particle_ref.show()

#%% Beam settings - from Hannes' and Isabelle's table 
bunch_intensity = 3.5e8 #8.1e8
sigma_z = 4.74
nemitt_x= 0.8e-6
nemitt_y= 0.5e-6 

# Analytical tune footprint settings 
Qh = 6.210000511153454
Qv = 6.24499998913536
plot_range  =   [[5.95,6.3],[5.95,6.3]]   # range in Qh & Qv for the plot
plot_order  =   5   # order of resonances to plot
periodicity =   16  # periodicity of ring for the colorcode of the plot

#%% Set space charge parameters and install in line 

num_spacecharge_interactions = 540   # number of SC kicks per turn 
tol_spacecharge_position = 1e-2   # tolerance in meters by how much location at which space charge kick is given can move 
mode = 'frozen'  # Available modes: frozen/quasi-frozen/pic

lprofile = xf.LongitudinalProfileQGaussian(
        number_of_particles=bunch_intensity,
        sigma_z=sigma_z,
        z0=0.,
        q_parameter=1.)


xf.install_spacecharge_frozen(line=line,
                   particle_ref=particle_ref,
                   longitudinal_profile=lprofile,
                   nemitt_x=nemitt_x, nemitt_y=nemitt_y,
                   sigma_z=sigma_z,
                   num_spacecharge_interactions=num_spacecharge_interactions,
                   tol_spacecharge_position=tol_spacecharge_position)


#%% Build tracker and perform Twiss on optics
line.build_tracker()
line.optimize_for_tracking()
line_sc_off = line.filter_elements(exclude_types_starting_with='SpaceCh')
twiss_xtrack = line_sc_off.twiss()  # optics adapted for sequence w/o SC
twiss_xtrack_with_sc = line.twiss()

#%% Generate matched beam of particles 
# Match Gaussian beam to optics in tracker without space charge
particles = xp.generate_matched_gaussian_bunch(
         num_particles=num_particles, total_intensity_particles=bunch_intensity,
         nemitt_x=nemitt_x, nemitt_y=nemitt_y, sigma_z=sigma_z,
         particle_ref=particle_ref, line=line_sc_off)

N_footprint = len(particles.x)

#%% Analytical tune shift from PySCRDT

# Create instance of PySCRDT, taking normalized emittances as input
s = PySCRDT()
s.setParameters(
    intensity = bunch_intensity,
    bunchLength = sigma_z,
    emittance_x = nemitt_x,
    emittance_y = nemitt_y, 
    dpp_rms = np.mean(particles.delta),  # very small contribution anyway to beam size
    bF=None,
    ro = particle_ref.get_classical_particle_radius0() 
)
s.loadTwissFromXsuite(twissTableXsuite=twiss_xtrack)

# caclulate detuning coefficients using potentials up to 20th order (needed for up to 3 sigma particles)
detuning=[]
# order in x
for i in range(0,21,2):
    print("--------- PySCRDT X order {} ------------- ".format(i))
    # order in y
    for j in range(0,21,2): 
        print("PySCRDT Y order {}".format(j))
        if (i==0) and (j==0):
            pass
        elif i+j<21:
            s.setOrder([int(i),int(j),'any'])
            s.potential()
            s.detuning()
            detuning.append([i,j,s.getDetuning()])
    
detuning=np.array(detuning)

#  initialize grid for calculation
s_N=6
s_max=3
theta_N=5

def initial_xy_polar(s_max, s_N, theta_N):
    return np.array(
        [
            [(s*np.cos(theta), s*np.sin(theta)) for s in np.linspace(0, s_max, s_N+1)]
            for theta in np.linspace(0, np.pi/2., theta_N)
        ])

S = initial_xy_polar(s_max=s_max, s_N=s_N, theta_N=theta_N)

#  estimate tunes from the detuning coefficients


en_x=s.parameters['emittance_x']
en_y=s.parameters['emittance_y']
beta=s.parameters['b']
gamma=s.parameters['g']
J_x=S[:,:,0]**2*en_x/2./beta/gamma
J_y=S[:,:,1]**2*en_y/2./beta/gamma

Qx,Qy = Qh, Qv 

for x_q,y_q,detuning_coef in detuning:
    if x_q:
        Qx+=x_q/2.*detuning_coef*(J_x**(x_q/2.-1))*(J_y**(y_q/2.))
    if y_q:
        Qy+=y_q/2.*detuning_coef*(J_y**(y_q/2.-1))*(J_x**(x_q/2.))

Q = np.dstack(
    (
        [qx.tolist() + [Qh] for qx in Qx],
        [qy.tolist() + [Qv] for qy in Qy],
    )
)

Q[:,:,0] += 0.00
Q[:,:,1] += 0.00

sx = Q.shape[0]-1
sy = Q.shape[1]-1
p1 = Q[:-1, :-1, :].reshape(sx*sy, 2)[:, :]
p2 = Q[1:, :-1, :].reshape(sx*sy, 2)[:]
p3 = Q[1:, 1:, :].reshape(sx*sy, 2)[:]
p4 = Q[:-1, 1:, :].reshape(sx*sy, 2)[:]


# do the plotting
cmap_base = plt.cm.hot
c_indcs = np.int_(np.linspace(0.1,0.6,s_N+1)*cmap_base.N)
cmap = colors.ListedColormap([cmap_base(c_indx) for c_indx in c_indcs])

# Stack endpoints to form polygons
Polygons = np.transpose(np.stack((p1, p2, p3, p4)), (1, 0, 2))
patches = list(map(matplotlib.patches.Polygon, Polygons))
p_collection = matplotlib.collections.PatchCollection(
#     patches, edgecolor='grey', linewidth=1,
    patches, edgecolor='k', linewidth=0.5,
#     facecolors=[],
    facecolors=cmap.colors,
#     facecolors=['SkyBlue'],
    alpha=0.7
)

detuning_y=[i for i in np.where(detuning[:,1]==2)[0] if i in np.where(detuning[:,0]==0)[0]][0]
detuning_x=[i for i in np.where(detuning[:,0]==2)[0] if i in np.where(detuning[:,1]==0)[0]][0]
print('dQx = ', str(detuning[detuning_x,2]))
print('dQy = ', str(detuning[detuning_y,2]))


#%% Track the particles turn-by-turn
#ctx2arr = context.nparray_from_context_array  # Copies an array to the device to a numpy array.
x_tbt = np.zeros((N_footprint, num_turns), dtype=np.float64)
y_tbt = np.zeros((N_footprint, num_turns), dtype=np.float64)
px_tbt = np.zeros((N_footprint, num_turns), dtype=np.float64)
py_tbt = np.zeros((N_footprint, num_turns), dtype=np.float64)

# Also for the non-dispersive components
x_tbt_ndc = np.zeros((N_footprint, num_turns), dtype=np.float64)
y_tbt_ndc = np.zeros((N_footprint, num_turns), dtype=np.float64)
px_tbt_ndc = np.zeros((N_footprint, num_turns), dtype=np.float64)
py_tbt_ndc = np.zeros((N_footprint, num_turns), dtype=np.float64)

epsn_x = np.zeros(num_turns)
epsn_y = np.zeros(num_turns)

# Define object to calculate statistical emittance 
em = statisticalEmittance(particles)

# Perform tracking
for ii in range(num_turns):
    print(f'Turn: {ii}\n', end='\r', flush=True)
    x_tbt[:, ii] = particles.x[:N_footprint].copy()
    y_tbt[:, ii] = particles.y[:N_footprint].copy()
    px_tbt[:, ii] = particles.px[:N_footprint].copy()
    py_tbt[:, ii] = particles.py[:N_footprint].copy()
    em.setInputDistribution(particles)
    epsn_x[ii] = em.getNormalizedEmittanceX()
    epsn_y[ii] = em.getNormalizedEmittanceY()
    
    # Calculate the coordinates and beam matrix excluding dispersive components
    em.betatronicMatrices()
    x_tbt_ndc[:, ii] = em.coordinateMatrixBetatronic[0]
    px_tbt_ndc[:, ii] = em.coordinateMatrixBetatronic[1]
    y_tbt_ndc[:, ii] = em.coordinateMatrixBetatronic[2]
    py_tbt_ndc[:, ii] = em.coordinateMatrixBetatronic[3]
    
    line.track(particles)

#%%  ########## Frequency analysis part #####################
Qx = np.zeros(N_footprint)
Qy = np.zeros(N_footprint)
Qx_n = np.zeros(N_footprint)  # tune footprint from normalized dispersion-corrected coordinates
Qy_n = np.zeros(N_footprint)
Qx_complex = np.zeros(N_footprint) # tune footprint from COMPLEX normalized dispersion-corrected coordinates
Qy_complex = np.zeros(N_footprint) 

# Also store values for the corrected footprint, picking out all frequencies
Qx_corrected = np.zeros(N_footprint)
Qy_corrected = np.zeros(N_footprint)
Qx_n_corrected = np.zeros(N_footprint)  # tune footprint from normalized dispersion-corrected coordinates
Qy_n_corrected = np.zeros(N_footprint)
Qx_complex_corrected = np.zeros(N_footprint) # tune footprint from COMPLEX normalized dispersion-corrected coordinates
Qy_complex_corrected = np.zeros(N_footprint) 

# Also store the coordinates from the betatronic matrices - also store the complex value
Qx_ndc = np.zeros(N_footprint)
Qy_ndc = np.zeros(N_footprint)
Qx_ndc_complex = np.zeros(N_footprint)
Qy_ndc_complex = np.zeros(N_footprint)

# Before finding the tune, normalize coordinates and remove dispersive parts
# 1) Remove closed orbit
x_tbt -= twiss_xtrack_with_sc['x'][0]
px_tbt -= twiss_xtrack_with_sc['px'][0]
y_tbt -= twiss_xtrack_with_sc['y'][0]
py_tbt -= twiss_xtrack_with_sc['py'][0]

# 2) Remove dispersive component - remove dispersion*delta from each particle
x_tbt_noDP = x_tbt - twiss_xtrack_with_sc['dx'][0]*(x_tbt.transpose() - particles.delta).transpose()
px_tbt_noDP = px_tbt - twiss_xtrack_with_sc['dpx'][0]*(px_tbt.transpose() - particles.delta).transpose()
y_tbt_noDP = y_tbt - twiss_xtrack_with_sc['dy'][0]*(y_tbt.transpose() - particles.delta).transpose()
py_tbt_noDP = py_tbt - twiss_xtrack_with_sc['dpy'][0]*(py_tbt.transpose() - particles.delta).transpose()

# 3) Normalize the coordinates
xn_tbt = x_tbt_noDP/np.sqrt(twiss_xtrack_with_sc['betx'][0])
pxn_tbt = -twiss_xtrack_with_sc['alfx'][0]/np.sqrt(twiss_xtrack_with_sc['betx'][0])*x_tbt_noDP \
          + np.sqrt(twiss_xtrack_with_sc['betx'][0])*px_tbt_noDP
yn_tbt = y_tbt_noDP/np.sqrt(twiss_xtrack_with_sc['bety'][0])
pyn_tbt = -twiss_xtrack_with_sc['alfy'][0]/np.sqrt(twiss_xtrack_with_sc['bety'][0])*y_tbt_noDP \
          + np.sqrt(twiss_xtrack_with_sc['bety'][0])*py_tbt_noDP

# 4) Create complex coordinates for frequency analysis 
complex_x = xn_tbt - 1.j*pxn_tbt
complex_y = yn_tbt - 1.j*pyn_tbt

complex_x_ndc = x_tbt_ndc - 1.j*px_tbt_ndc
complex_y_ndc = y_tbt_ndc - 1.j*py_tbt_ndc

# Set threshold for synchrotron radiation frequency to filter out
Qmin = 0.05

# Add the estimates tunes - also remember to subtract mean 
for i_part in range(N_footprint):
    
    # Method 1: get dominant tune from x_tbt data and from the complex x_tbt px_tbt pair
    Qx[i_part] = NAFFlib.get_tune(x_tbt[i_part, :] - np.mean(x_tbt[i_part, :]))  
    Qy[i_part] = NAFFlib.get_tune(y_tbt[i_part, :] - np.mean(y_tbt[i_part, :]))  
    Qx_n[i_part] = NAFFlib.get_tune(xn_tbt[i_part, :] - np.mean(xn_tbt[i_part, :]))  
    Qy_n[i_part] = NAFFlib.get_tune(yn_tbt[i_part, :] - np.mean(yn_tbt[i_part, :])) 
    Qx_complex[i_part] = NAFFlib.get_tune(complex_x[i_part, :])
    Qy_complex[i_part] = NAFFlib.get_tune(complex_y[i_part, :])
    
    # Method 2: pick out maximum tune from x_tbt data and from the complex x_tbt px_tbt pair, correcting for synchrotron tune 
    Qx_corr = NAFFlib.get_tunes(x_tbt[i_part, :] - np.mean(x_tbt[i_part, :]), 2)[0]
    Qx_corrected[i_part] = Qx_corr[np.argmax(Qx_corr>Qmin)]  # find most dominant tune larger than this value
    
    Qy_corr = NAFFlib.get_tunes(y_tbt[i_part, :] - np.mean(y_tbt[i_part, :]), 2)[0]
    Qy_corrected[i_part] = Qy_corr[np.argmax(Qy_corr>Qmin)]
    
    Qx_n_corr = NAFFlib.get_tunes(xn_tbt[i_part, :] - np.mean(xn_tbt[i_part, :]), 2)[0]
    Qx_n_corrected[i_part] = Qx_n_corr[np.argmax(Qx_n_corr>Qmin)]
    
    Qy_n_corr = NAFFlib.get_tunes(yn_tbt[i_part, :] - np.mean(yn_tbt[i_part, :]), 2)[0]
    Qy_n_corrected[i_part] = Qy_n_corr[np.argmax(Qy_n_corr>Qmin)]
    
    Qx_complex_corr = NAFFlib.get_tunes(complex_x[i_part, :], 2)[0]
    Qx_complex_corrected[i_part] = Qx_complex_corr[np.argmax(Qx_complex_corr>Qmin)]
    
    Qy_complex_corr = NAFFlib.get_tunes(complex_y[i_part, :], 2)[0]
    Qy_complex_corrected[i_part] =  Qy_complex_corr[np.argmax(Qy_complex_corr>Qmin)]
    
    
    # Method 3: pick out dominant tune from dispersion-corrected betatronic matrix data, also comparing with complex x_tbt px_tbt pair
    Qx_ndc_tune = NAFFlib.get_tunes(x_tbt_ndc[i_part, :] - np.mean(x_tbt_ndc[i_part, :]), 2)[0]  
    Qx_ndc[i_part] = Qx_ndc_tune[np.argmax(Qx_ndc_tune>Qmin)]
    
    Qy_ndc_tune = NAFFlib.get_tunes(y_tbt_ndc[i_part, :] - np.mean(y_tbt_ndc[i_part, :]), 2)[0]  
    Qy_ndc[i_part] = Qy_ndc_tune[np.argmax(Qy_ndc_tune>Qmin)]
    
    Qx_ndc_complex_tune = NAFFlib.get_tunes(complex_x_ndc[i_part, :], 2)[0]  
    Qx_ndc_complex[i_part] = Qx_ndc_complex_tune[np.argmax(Qx_ndc_complex_tune>Qmin)]
    
    Qy_ndc_complex_tune = NAFFlib.get_tunes(complex_y_ndc[i_part, :], 2)[0]  
    Qy_ndc_complex[i_part] = Qy_ndc_complex_tune[np.argmax(Qy_ndc_complex_tune>Qmin)]
    

# Find indices of particles that seem to be affected by noise for method 1
noise_index = np.where(Qx < 0.05)[0]
noise_index_ndc = np.where(Qx_ndc < 0.05)[0]

# Maximum tune shift - remove any outlying particles trapped by synchrotron oscillartions
dQx_track = np.min(Qx_ndc[np.where(Qx_ndc > 0.05)[0]])-(twiss_xtrack['qx'] % 1)
dQy_track = np.min(Qy_ndc)-(twiss_xtrack['qy'] % 1)
print("\nPySCRDT SC tune shift:        dQx = {}, dQy = {}".format(detuning[detuning_x,2], detuning[detuning_y,2]))
print('-' * 90)
print("\nMax tune shift from tracking: dQx = {}, dQy = {}".format(dQx_track, dQy_track))
    
#%% Plotting 

matplotlib.rcParams['axes.labelsize'] = 20
matplotlib.rcParams['xtick.labelsize'] = 14
matplotlib.rcParams['ytick.labelsize'] = 14
matplotlib.rcParams["savefig.dpi"] = 250

plt.close('all')

box = {'facecolor': 'none',
       'edgecolor': 'green',
       'boxstyle': 'round'
      }

# Plot the optics functions
fig0 = plt.figure(figsize=(10,7))
acc_lib.plot_tools.plot_twiss(fig0, twiss_xtrack)
plt.text(.01, .99, 'NO Space charge kick', ha='left', va='top') #, transform=ax.transAxes)
fig0.savefig('PS_Pb_ions_first_footprint_plots/twiss_no_space_charge.png', dpi=250)

# Plot the optics functions
fig1 = plt.figure(figsize=(10,7))
acc_lib.plot_tools.plot_twiss(fig1, twiss_xtrack_with_sc)
plt.text(.01, .99, 'Space charge kick', ha='left', va='top') #, transform=ax.transAxes)
fig1.savefig('PS_Pb_ions_first_footprint_plots/twiss_wtih_space_charge.png', dpi=250)


#### FIRST PLOT TUNE FOOTPRINT FROM METHOD 1: SYNCHROTRON TUNES WILL APPEAR #########

fig2 = plt.figure(figsize=(10,7))
ax2 = fig2.add_subplot(1,1,1)
fig2.suptitle('Method 1 - Tune footprint: un-normalized (red), \nnormalized (green) and complex (blue) coords', fontsize=16)
ax2.plot(Qx+6.0, Qy+6.0, 'go', markersize=7.5, alpha=0.4)
ax2.plot(Qx_n+6.0, Qy_n+6.0, 'ro', markersize=4.5, alpha=0.2)
ax2.plot(Qx_complex+6.0, Qy_complex+6.0, 'bo', markersize=1.5, alpha=0.2)
ax2.set_ylabel('$Q_{y}$')
ax2.set_xlabel('$Q_{x}$')
fig2.savefig('PS_Pb_ions_first_footprint_plots/Method1_Tune_Footprint.png', dpi=250)

# For the normal turn-by-turn data: Compare two particles inside and outside the tune footprint
fig4 = plt.figure(figsize=(10,7))
fig4.suptitle('Method 1 - Centroid tracking')
ax4 = fig4.add_subplot(2, 1, 1)  # create an axes object in the figure
ax4.plot(x_tbt[-1, :], marker='o', color='b', markersize=3, label='Footprint particle')
ax4.plot(x_tbt[noise_index[0], :], marker='o', color='r', markersize=3, label='Particle outside')
ax4.set_ylabel("Centroid $X$ [m]")
ax4.set_xlabel("#turns")
ax5 = fig4.add_subplot(2, 1, 2)  # create a second axes object in the figure
ax5.plot(y_tbt[-1, :], marker='o', color='b', markersize=3, label='Footprint particle')
ax5.plot(y_tbt[noise_index[0], :], marker='o', color='r', markersize=3, label='Particle outside')
ax5.set_ylabel("Centroid $Y$ [m]")
ax5.set_xlabel("#turns")
ax5.legend(fontsize=16)
fig4.savefig('PS_Pb_ions_first_footprint_plots/Method1_Centroid_Tracking.png', dpi=250)

#### THEN PLOT TUNE FOOTPRINT FROM METHOD 2: SYNCHROTRON TUNES STILL APPEARS DESPITE (FAULTY) CORRECTION #########

fig22 = plt.figure(figsize=(10,7))
ax22 = fig22.add_subplot(1,1,1)
fig22.suptitle('Method 2: Corrected Tune footprint: un-normalized (red), \nnormalized (green) and complex (blue) coords', fontsize=16)
ax22.plot(Qx_corrected+6.0, Qy_corrected+6.0, 'go', markersize=7.5, alpha=0.4)
ax22.plot(Qx_n_corrected+6.0, Qy_n_corrected+6.0, 'ro', markersize=4.5, alpha=0.2)
ax22.plot(Qx_complex_corrected+6.0, Qy_complex_corrected+6.0, 'bo', markersize=1.5, alpha=0.2)
ax22.set_ylabel('$Q_{y}$')
ax22.set_xlabel('$Q_{x}$')
fig22.savefig('PS_Pb_ions_first_footprint_plots/Method2_Tune_Footprint.png', dpi=250)

fig5 = plt.figure(figsize=(10,9))
ax5 = fig5.add_subplot(2,1,1)
ax6 = fig5.add_subplot(2,1,2)
fig5.suptitle('Method 2: Normalized Phase Space', fontsize=20)
ax5.plot(xn_tbt, pxn_tbt, 'go', markersize=3.5, alpha=0.2) #, label='Normalized X')
ax6.plot(yn_tbt, pyn_tbt, 'ro', markersize=3.5, alpha=0.2) #, label='Normalized Y')
ax5.set_ylabel('$\\bar{P}_{x}$')
ax5.set_xlabel('$\\bar{X}$')
ax6.set_ylabel('$\\bar{P}_{y}$')
ax6.set_xlabel('$\\bar{Y}$')
fig5.savefig('PS_Pb_ions_first_footprint_plots/Method2_Normalized_Phase_Space.png', dpi=250)

#### PLOT TUNE FOOTPRINT FROM METHOD 3 WITH BETATRONIC MATRICES WITH DISPERSION CORRECTION: NO SYNCHROTRON TUNES #########

fig23 = plt.figure(figsize=(10,7))
fig23.suptitle('Method 3: Tune footprint: betatronic matrices \n D-corrected (green) and D-corrected complex (blue)', fontsize=16)
ax23 = fig23.add_subplot(1,1,1)
ax23.plot(Qx_ndc+6.0, Qy_ndc+6.0, 'go', markersize=4.5, alpha=0.2)
ax23.plot(Qx_ndc_complex+6.0, Qy_ndc_complex+6.0, 'bo', markersize=1.5, alpha=0.2)
ax23.set_ylabel('$Q_{y}$')
ax23.set_xlabel('$Q_{x}$')
fig23.savefig('PS_Pb_ions_first_footprint_plots/Method3_Tune_Footprint.png', dpi=250)

# Compare particle oscillation from betatronic matrices
ind_min = np.argmin(np.abs(x_tbt_ndc[:, 0]))  # check particle with smallest starting amplitude - any synchrotron oscillations there? 

fig41 = plt.figure(figsize=(10,7))
fig41.suptitle('Method 3: Betatronic matrix: Centroid checks')
ax41 = fig41.add_subplot(2, 1, 1)  # create an axes object in the figure
ax41.plot(x_tbt_ndc[0, :], marker='o', color='b', markersize=3, label='First footprint particle')
ax41.plot(x_tbt_ndc[ind_min, :], marker='o', color='r', markersize=3, label='Zero start amplitude particle')
ax41.set_ylabel("Centroid $X$ [m]")
ax41.set_xlabel("#turns")
ax51 = fig41.add_subplot(2, 1, 2)  # create a second axes object in the figure
ax51.plot(y_tbt_ndc[0, :], marker='o', color='b', markersize=3, label='First footprint particle')
ax51.plot(y_tbt_ndc[ind_min, :], marker='o', color='r', markersize=3, label='Zero start amplitude particle')
ax51.set_ylabel("Centroid $Y$ [m]")
ax51.set_xlabel("#turns")
ax51.legend(fontsize=16)
fig41.savefig('PS_Pb_ions_first_footprint_plots/Method3_Centroid_Tracking.png', dpi=250)

# Also check dispersion-corrected phase space
fig51 = plt.figure(figsize=(10,9))
ax51 = fig51.add_subplot(2,1,1)
ax61 = fig51.add_subplot(2,1,2)
fig51.suptitle('Method 3: Dispersion-corrected Phase Space', fontsize=20)
ax51.plot(x_tbt_ndc, px_tbt_ndc, 'go', markersize=3.5, alpha=0.2) 
ax61.plot(y_tbt_ndc, py_tbt_ndc, 'ro', markersize=3.5, alpha=0.2) 
ax51.set_ylabel('$P_{x}$')
ax51.set_xlabel('$X$')
ax61.set_ylabel('$P_{y}$')
ax61.set_xlabel('$Y$')
fig51.savefig('PS_Pb_ions_first_footprint_plots/Method3_Phase_Space.png', dpi=250)

# For one of these particles - do a simple FFT to check what tunes that dominate
fig00 = plt.figure(figsize=(10,7))
ax = fig00.add_subplot(1, 1, 1)  # create an axes object in the figure
num_turns = len(x_tbt_ndc[0])
x_fft = fftfreq(num_turns)[:num_turns//2]  # integer division
y_fft = 2*np.abs(fft(x_tbt_ndc[0]))/num_turns
ax.set_xlabel("Fractional horizontal tune")
fig00.suptitle('Method 3: FFT spectrum of tracking',fontsize=18)
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)  
plt.yticks(fontsize=14)
ax.plot(x_fft, y_fft[:int(num_turns/2)])  # only plot positive frequencies, i.e. first half of vector   
ax.set_ylabel("Amplitude [m]")
fig00.tight_layout()
fig00.savefig('PS_Pb_ions_first_footprint_plots/Method3_FFT.png', dpi=250)

# Then also plot this tune footprint with the analytical tune footprint
fig333, ax = plt.subplots(1,figsize=(10, 10))
fig333.suptitle('Method 3: Tune footprint, tracking and analytical', fontsize=20)
tune_diagram = resonance_lines.resonance_lines(plot_range[0],
            plot_range[1], np.arange(1, plot_order+1), periodicity)
tune_diagram.plot_resonance(figure_object=fig333, interactive=False)
#fig.suptitle('PS Pb ion', fontsize=22)
ax.plot(Qx_ndc+6.0, Qy_ndc+6.0, 'go', markersize=3.5, alpha=0.3)
ax.get_xaxis().get_major_formatter().set_useOffset(False)
ax.get_yaxis().get_major_formatter().set_useOffset(False)
ax.add_collection(p_collection)
ax.set_aspect('equal')
plt.tight_layout()
#plt.savefig(input_parameters.figure, dpi=250)
plt.show()
fig333.savefig('PS_Pb_ions_first_footprint_plots/Method3_Tune_Footprint_with_Analytical.png', dpi=250)
