"""
SEQUENCE GENERATOR FOR PS WITH PB IONS - FLAT BOTTOM

Script to set up PS sequence with MADX using 2022 optics from acc-models/PS, also performing a sanity check between the two,
also generating .json and .seq files with for X-suite use

by Elias Waagaard 
"""
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

import xobjects as xo
import xtrack as xt
import xpart as xp

from cpymad.madx import Madx

import json
import os
import sys

# Also import personal collection of functions for accelerator physics 
import acc_lib

# Define sequence name and optics to import model from AFS
seq_name = 'ps'
optics = '/home/elwaagaa/cernbox/PhD/Projects/acc-models-ps'

# Flags
save_seq_to_json = True # whether to save sequence for facilitated future tracking 
save_madx_seq = True
test_ptc_tracking = True
save_fig = True
x_suite_matched_Gaussian = True  # to use matched Gaussian or custom beam for tracking


# Define parameters for tracking - Pb beam parameters from Hannes' and Isabelle's 2021 table
num_turns = 100
num_particles = 1000
bunch_intensity = 8.1e8
sigma_z = 4.74
nemitt_x= 0.8e-6
nemitt_y= 0.5e-6  


#%% Set up the X-suite contexts
context = xo.ContextCpu()
buf = context.new_buffer()   # using default initial capacity

#%% Initiate MADX sequence and call the sequence and optics file
#madx = Madx(stdout=False)
with open('tempfile', 'w') as f:
    madx = Madx(stdout=True)    

# Call the relevant madx files and macros  
madx.call("{}/_scripts/macros.madx".format(optics))
madx.call("{}/scenarios/lhc_ion/1_flat_bottom/ps_fb_ion.beam".format(optics))   
madx.input('''BRHO      := BEAM->PC * 3.3356 * 208./54.;''')
madx.call("{}/ps_mu.seq".format(optics))
madx.call("{}/ps_ss.seq".format(optics))
madx.call("{}/scenarios/lhc_ion/1_flat_bottom/ps_fb_ion.str".format(optics))

""" OLDER VERSION OF CALLING FILES THROUGH AFS
madx.call("ps/_scripts/macros.madx", chdir=True)
madx.call("ps/scenarios/lhc_ion/1_flat_bottom/ps_fb_ion.beam", chdir=True)   
madx.input('''BRHO      := BEAM->PC * 3.3356 * 208./54.;''')
madx.call("ps/ps_mu.seq")
madx.call("ps/ps_ss.seq")
madx.call("ps/scenarios/lhc_ion/1_flat_bottom/ps_fb_ion.str")
"""

fig_str = 'Pb_ions'


# Perform Twiss with thick-element sequence 
madx.use(sequence='ps')
madx.command.flatten()  # flatten to unnested the combined function magnets 
twiss = madx.twiss()


# Plot the Twiss 
fig1 = plt.figure(figsize=(10,7))
acc_lib.plot_tools.plot_twiss(fig1, twiss, twiss_from_madx=True)
fig1.text(0.01, 0.93, 'MADX Twiss Thick', fontsize=22)
fig1.savefig('PS_PB_Ion_flat_bottom_TwissThick.png', dpi=250)


# Save MAD-X table for reference use, e.g. to compare with PySCRDT
madx.input('''
           use,sequence=ps;
           twiss,file="Twiss_tables/ps_{}_thick.tfs";
        '''.format(fig_str))

# Save thick sequence
if save_madx_seq:
    madx.command.save(sequence='ps', file='PS_sequence/PS_2022_Pb_ions_thick.seq', beam=True)


#%% Create a line for X-suite from MADX sequence - also rematch tunes and chromaticities

madx.input('''
           SEQEDIT, SEQUENCE=PS;
           FLATTEN;
           REFER=CENTRE;
           FLATTEN;
           ''')

madx.use(sequence='ps')
# Slice and match tunes
n_slice_per_element = 10
seq_name = 'ps'

# Try without the matching, then the chro
madx.input(f'''
select, flag=MAKETHIN, SLICE={n_slice_per_element}, thick=false;
MAKETHIN, SEQUENCE={seq_name}, MAKEDIPEDGE=false;
use, sequence={seq_name};

MATCH, Sequence=PS;

              VARY, NAME = **kf**, STEP = 1e-3;

              VARY, NAME = **kd**, STEP = 1e-3;

              GLOBAL, Q1 = QH;

              GLOBAL, Q2 = QV;

              JACOBIAN,CALLS=1000,TOLERANCE=1.0E-18,STRATEGY=3;

ENDMATCH;

''')

# Try matching the chromas with thep pole face windings 
madx.input(''' 
DQH = -5.267154857;
DQV = -7.199237456;
           
use, sequence=PS;
MATCH, Sequence=PS;

              VARY, NAME = **k2PRPFWF,**, STEP = 1e-3;

              VARY, NAME = **k2PRPFWD**, STEP = 1e-3;

              GLOBAL, DQ1 = DQH;

              GLOBAL, DQ2 = DQV;

              JACOBIAN,CALLS=1000,TOLERANCE=1.0E-18,STRATEGY=3;

ENDMATCH;
           ''')


#"""
# Test the thin Twiss with MADX 
madx.use(sequence='ps')
twiss2 = madx.twiss()
fig = plt.figure(figsize=(10,7))
acc_lib.plot_tools.plot_twiss(fig, twiss2, twiss_from_madx=True)
fig.text(0.01, 0.93, 'MADX Twiss Thin', fontsize=22)
fig.savefig('PS_PB_Ion_flat_bottom_TwissThin_MADX.png', dpi=250)
#"""

madx.use(sequence='ps')
line = xt.Line.from_madx_sequence(madx.sequence[seq_name])
madx_beam = madx.sequence[seq_name].beam

particle_sample = xp.Particles(
        p0c = madx_beam.pc*1e9,
        q0 = madx_beam.charge,
        mass0 = madx_beam.mass*1e9)

line.particle_ref = particle_sample



#%% Perform Twiss command from tracker
tracker = xt.Tracker(_context=context, _buffer=buf, line=line) 
twiss_xtrack = tracker.twiss(method='4d')  # we only look at transverse coordinates
Qh_int, Qv_int = int(twiss_xtrack['qx']), int(twiss_xtrack['qy'])  # integer tune 
Qh, Qv = round(twiss_xtrack['qx'] % 1, 2), round(twiss_xtrack['qy'] % 1, 2)  # fractional working point, two decimal points
Qh_set = Qh_int + Qh
Qv_set = Qv_int + Qv

# Plot the Twiss 
fig2 = plt.figure(figsize=(10,7))
acc_lib.plot_tools.plot_twiss(fig2, twiss_xtrack, twiss_from_madx=False)
fig2.text(0.01, 0.93, 'Xtrack Twiss Thin', fontsize=22)
fig2.savefig('PS_PB_Ion_flat_bottom_TwissThin.png', dpi=250)

#%% Save sequences, either X-track or MAD-X 
if save_seq_to_json:
    with open('PS_sequence/PS_2022_Pb_ions.json', 'w') as fid:
        json.dump(line.to_dict(), fid, cls=xo.JEncoder)

if save_madx_seq:
    madx.command.save(sequence='ps', file='PS_sequence/PS_2022_Pb_ions_thin.seq', beam=True)

#####################################################
############ Sanity checks MADX vs Xsuite: PTC track
#####################################################
# Test PTC tracking for the line
beta0 = line.particle_ref.beta0[0]

if test_ptc_tracking:
    common = list(set(line.element_names).intersection(madx.sequence.ps.element_names()))
    
    madx.input(f'''
    ptc_create_universe;
    ptc_create_layout,model=2,method=6,nst=10,exact;
    
    ptc_start, x= 1e-6, px=1e-6, y= 1e-6, py=1e-6;
    ''')
    #for element in madx.sequence['psb'].element_names():
    for element in common:
        #if element.startswith('marker'):
        madx.input(f'''
        ptc_observe, PLACE={element};
        ''')
    madx.input(f'''
    ptc_track,icase=4,closed_orbit, element_by_element, ,dump,
           turns= 1,ffile=1, onetable;!, turns=1000, norm_no=4; norm_out
    ptc_track_end;
    ptc_end;
    ''')
    
    ptc_track = pd.read_csv( "trackone", delim_whitespace=True, skiprows=9, header=None)
    ptc_track = ptc_track[ptc_track[0].str.contains('segment') == False]
    ptc_track.columns = ["NUMBER","TURN","X","PX","Y","PY","T", "PT", "S", "E"]
    ptc_track = ptc_track.drop_duplicates(subset=['S'])
    
    mad_beam = madx.sequence['ps'].beam
    particles_3 = xp.Particles(
            mass0=mad_beam.mass*1e9, gamma0=mad_beam.gamma, q0 = mad_beam.charge, p0c = mad_beam.pc*1e9,#7e12, # 7 TeV
            x=[1e-6], px=[1e-6], y=[1e-6], py=[1e-6],
            zeta=[0], delta=[0])
    
    tracker.track(particles_3, turn_by_turn_monitor='ONE_TURN_EBE')
    
    df_track = pd.DataFrame({"s": tracker.record_last_track.s[0][:-1],
                             "x": tracker.record_last_track.x[0][:-1],
                             "ptau": tracker.record_last_track.ptau[0][:-1],
                             "delta":tracker.record_last_track.delta[0][:-1],
                             "name": line.element_names })
    
    df_track = df_track[df_track.name.isin(common) ]
    df_track = df_track.drop_duplicates(subset=['s'])
    
    
    fig, ax = plt.subplots(nrows=2, figsize=(10, 8))
    fig.suptitle("PS Pb Ion Tracking: PTC vs Xtrack", fontsize=22)
    plt.sca(ax[0])
    plt.plot(ptc_track.S.values, ptc_track.X.values, label='PTC', marker='o', ms=3)
    plt.plot(df_track.s, df_track.x, label='XTRACK', marker='o', ms=3)
    #plt.xlabel("s (m)")
    plt.legend()
    plt.ylabel("x")
    plt.sca(ax[1])
    #ax2 = ax[1].twinx()
    #plt.ylabel("Diff ptc xtrack in s")
    #plt.plot(ptc_track.S.values, ptc_track.S.values-df_track.s.values, c='g', lw=0.5)
    plt.sca(ax[1])
    plt.plot(ptc_track.S.values, ptc_track.X.values-df_track.x.values, label='PTC', marker='o', ms=3, color='r')
    plt.xlabel("s (m)")
    plt.ylabel("Delta x")
    #ax[0].set_xlim(2925, 3040)   # to compare small offset 
    #ax[1].set_xlim(2925, 3040)
    if save_fig:
        fig.savefig("Plots/PS_Pb_horizontal_ions_comparison_ptc_xsuite.png")
    
    # Check the element locations:
    #df_track[(df_track.s < 2950) & (df_track.s > 2920)]
      
    # Plot turn-by-turn centroid from PTC tracking
    fig2, ax2 = plt.subplots(nrows=2, figsize=(10, 8))
    fig2.suptitle("PS Pb Tracking: PTC vs Xtrack", fontsize=22)
    plt.sca(ax2[0])
    #ax2[0].set_yscale('log')
    plt.plot(ptc_track.S.values, ptc_track.PT.values, label='PT from PTC', marker='o', ms=3)
    plt.plot(df_track.s, df_track.ptau, label='ptau from XTRACK', marker='o', ms=3)
    #plt.xlabel("s (m)")
    plt.legend()
    plt.ylabel("PT")
    plt.sca(ax2[1])
    plt.plot(ptc_track.S.values, ptc_track.PT.values-df_track.ptau.values, label='PTC', marker='o', ms=3, color='r')
    plt.xlabel("s (m)")
    plt.ylabel("Difference ptau vs PT")
    if save_fig:
        fig2.savefig("Plots/PS_DeltaP_ions_comparison_ptc_xsuite.png")

#Sanity checks from Gianni: Compare tunes, chromaticities and momentum compation factors
print("MAD-X:  " f"Qx  = {madx.table.summ['q1'][0]:.8f}"           f"   Qy  = {madx.table.summ['q2'][0]:.8f}")
print("Xsuite: " f"Qx  = {twiss_xtrack['qx']:.8f}"                        f"   Qy  = {twiss_xtrack['qy']:.8f}\n")
print("MAD-X:  " f"Q'x = {madx.table.summ['dq1'][0]*beta0:.7f}"    f"   Q'y = {madx.table.summ['dq2'][0]*beta0:.7f}")
print("Xsuite: " f"Q'x = {twiss_xtrack['dqx']:.7f}"                       f"   Q'y = {twiss_xtrack['dqy']:.7f}\n")
print("MAD-X:  " f"alpha_p = {madx.table.summ.alfa[0]:.7f}")
print("Xsuite: " f"alpha_p = {twiss_xtrack['momentum_compaction_factor']:.7f}")

madx.input("emit;")
print("Qs: " f"MADX={madx.table['emitsumm']['qs'][0]}" f" Xsuite={twiss_xtrack['qs']}\n") 