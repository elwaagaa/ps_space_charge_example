"""
Small test script to calculate maximum detuning from space charge (SC) with PS lattice in two different manners:

1. Run the PySCRDT module (https://github.com/fasvesta/PySCRDT/blob/master/PySCRDT/PySCRDT.py) to 
calculate maximum detuning from the space charge potential 

2. Using the linear (Laslett) tune shifts from Eq. 7 in Lee et al. 2006  (https://iopscience.iop.org/article/10.1088/1367-2630/8/11/291/pdf)
"""
import sys 
import numpy as np
import pandas as pd
import os 

import xobjects as xo
import xtrack as xt
import xpart as xp

from cpymad.madx import Madx
from cpymad import libmadx

from scipy.constants import m_p
from scipy.constants import e as qe
from scipy.constants import c as clight
from scipy.constants import epsilon_0

#%%  TUNE SHIFT WITH FOTEINI'S MODULE PYSCRDT 

# Import PySCRDT module from where it is located 
sys.path.append('/home/elwaagaa/cernbox/PhD/Python/PySCRDT/PySCRDT')
from PySCRDT import PySCRDT

# Decide whether beam is Gaussian or not
Gaussian = True
interpolate_over_lattice = True# flag to decide whether to increase the resolution of the lattice by interpolation

# PS test parameters from Isabelle's and Hannes' paper
intensity       =       8.1e8
bunchLength     =       4.74 #*0.8     #sigma in m  
emittance_x     =       0.8e-6
emittance_y     =       0.5e-6
dpp_rms         =       0.63e-3
if Gaussian:
    bF              =       None # None for Gaussian
else: 
    bF              =       0.174
    
# Calculate classical particle radius 
Q = 52  # partially stripped lead ion - if we set 82 then we get the r0 value from SPS 
A = 208
m_ion = 207.9766521 * 1.660539e-27 # Lead-208 mass in dalton, so convert to kilograms   #m_ion = 193.7 in GeV/c^2 
r0 = (Q*qe)**2/(4*np.pi*epsilon_0*m_ion*clight**2)  # classical particle radius, for ion 

# Initiate SC RDT calculator     
s = PySCRDT()
s.setParameters(
    intensity=intensity,
    bunchLength=bunchLength,
    emittance_x=emittance_x,
    emittance_y=emittance_y, 
    dpp_rms=dpp_rms,  
    bF=bF,
    ro = r0 
)
s.prepareData(twissFile='Twiss_tables/ps_Pb_ions_thick.tfs', skip_header_nr=50, skip_rows_nr=52)

# Check the vertical tune shift
s.setOrder([0,2,'any'])
s.potential()
s.detuning()
dQy_RDT = s.getDetuning()

# Check the horizontal tune shift
s.setOrder([2,0,'any'])
s.potential()
s.detuning()
dQx_RDT = s.getDetuning()


#%% Using analytical calculation and perveance formula from Lee et al. 2006

# Define generalized space-charge perveance depending on the beam type:
if Gaussian:
    K= 2*s.parameters['intensity']*s.parameters['ro']/(np.sqrt(2*np.pi)*s.parameters['bunchLength']*s.parameters['b']**2*s.parameters['g']**3)
else:
    K= 2*s.parameters['intensity']*s.parameters['ro']*(s.parameters['harmonic']/s.parameters['bF'])/(s.parameters['C']*s.parameters['b']**2*s.parameters['g']**3)
    
# Read data from PSB Twiss file 
header=np.genfromtxt('Twiss_tables/ps_Pb_ions_thick.tfs', skip_header=50,max_rows=1,dtype=str)
data=np.loadtxt(
                'Twiss_tables/ps_Pb_ions_thick.tfs',
                skiprows=52,
                usecols=(
                    np.where(header=='S')[0][0]-1,  
                    np.where(header=='BETX')[0][0]-1,
                    np.where(header=='BETY')[0][0]-1,
                    np.where(header=='DX')[0][0]-1,
                    np.where(header=='DY')[0][0]-1,
                    np.where(header=='MUX')[0][0]-1,
                    np.where(header=='MUY')[0][0]-1,
                    np.where(header=='L')[0][0]-1
                    )
                )
columns = ['s', 'betx', 'bety', 'dx', 'dy', 'mux', 'muy', 'l']
if interpolate_over_lattice:  # increase data points over accelerator for smoother data points
        ss = np.linspace(0,s.parameters['C'],100000)
        data2=np.zeros((100000,8))
        data2[:,1] = np.square(np.interp(ss,data[:,0],np.sqrt(data[:,1])))
        data2[:,2] = np.square(np.interp(ss,data[:,0],np.sqrt(data[:,2])))
        data2[:,3] = np.interp(ss,data[:,0],s.parameters['b']*data[:,3])
        data2[:,4] = np.interp(ss,data[:,0],s.parameters['b']*data[:,4])
        data2[:,5] = np.interp(ss,data[:,0],data[:,5])
        data2[:,6] = np.interp(ss,data[:,0],data[:,6])
        data2[:,7] += s.parameters['C']/len(ss)
        data2[:,0] = ss
        data = data2
twiss = pd.DataFrame(data, columns = columns)


#%% Calculate beam sizes
sigma_x = np.sqrt(s.parameters['emittance_x']*twiss['betx']/(s.parameters['b']*s.parameters['g'])+(s.parameters['dpp_rms']*twiss['dx'])**2)
sigma_y = np.sqrt(s.parameters['emittance_y']*twiss['bety']/(s.parameters['b']*s.parameters['g'])+(s.parameters['dpp_rms']*twiss['dy'])**2)

#%% Perform numerical path integration over the accelerator, and compare  
integrand_x = twiss['betx']/(sigma_x*(sigma_x + sigma_y))  # integrand for curve integral 
integrand_y = twiss['bety']/(sigma_y*(sigma_x + sigma_y))  # integrand for curve integral 

deltaQ_x = -K/(4*np.pi)*np.trapz(integrand_x, x=twiss['s'])
deltaQ_y = -K/(4*np.pi)*np.trapz(integrand_y, x=twiss['s'])    

print("PySCRDT SC tune shifts:          dQx = {}, dQy = {}".format(dQx_RDT, dQy_RDT))
print("Linear (Laslett) SC tune shifts: dQx = {}, dQy = {}".format(deltaQ_x, deltaQ_y))