"""
SEQUENCE GENERATOR FOR PS WITH PB IONS - FLAT BOTTOM

MATCHING USING THE MACROS ON THE PS-ACC-MODELS

Script to set up PS sequence with MADX using 2021 optics from acc-models/SPS, also performing a sanity check between the two,
also generating .json and .seq files with for X-suite use

by Elias Waagaard 
"""
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

import xobjects as xo
import xtrack as xt
import xpart as xp

from cpymad.madx import Madx

import json
import os
import sys

# Also import personal collection of functions for accelerator physics 
import acc_lib

# Define sequence name and optics to import model from AFS
seq_name = 'ps'
optics = "/afs/cern.ch/eng/acc-models/ps/2022"
if not os.path.isdir("ps"):
    os.symlink(optics, "ps")

# Flags
save_seq_to_json = True # whether to save sequence for facilitated future tracking 
save_madx_seq = True
test_xsuite_tracking = False
test_ptc_tracking = False
save_fig = False
x_suite_matched_Gaussian = True  # to use matched Gaussian or custom beam for tracking


# Define parameters for tracking
num_turns = 100
num_particles = 1000
bunch_intensity = 1e11/3
sigma_z = 22.5e-2/3 # Short bunch to avoid probing bucket non-linearityn to compare against frozen
nemitt_x=2.5e-6
nemitt_y=2.5e-6


#%% Set up the X-suite contexts
context = xo.ContextCpu()
buf = context.new_buffer()   # using default initial capacity

#%% Initiate MADX sequence and call the sequence and optics file
#madx = Madx(stdout=False)
with open('tempfile', 'w') as f:
    madx = Madx(stdout=True)    


# Call the relevant madx files and macros  
madx.call("ps/_scripts/macros.madx", chdir=True)
madx.call("ps/scenarios/lhc_ion/1_flat_bottom/ps_fb_ion.beam", chdir=True)   
madx.input('''BRHO      := BEAM->PC * 3.3356 * 208./54.;''')
madx.call("ps/ps_mu.seq")
madx.call("ps/ps_ss.seq")
madx.call("ps/scenarios/lhc_ion/1_flat_bottom/ps_fb_ion.str")
fig_str = 'Pb_ions'


# Perform Twiss with thick-element sequence 
madx.use(sequence='ps')
madx.command.flatten()  # flatten to unnested the combined function magnets 
twiss = madx.twiss()


# Plot the Twiss 
fig1 = plt.figure(figsize=(10,7))
acc_lib.plot_tools.plot_twiss(fig1, twiss, twiss_from_madx=True)
fig1.text(0.01, 0.93, 'MADX Twiss Thick', fontsize=22)
fig1.savefig('PS_PB_Ion_flat_bottom_TwissThick.png', dpi=250)


# Save MAD-X table for reference use, e.g. to compare with PySCRDT
madx.input('''
           use,sequence=sps;
           twiss,file="Twiss_tables/ps_{}_thick.tfs";
        '''.format(fig_str))

# Save thick sequence
if save_madx_seq:
    madx.command.save(sequence='ps', file='PS_sequence/PS_2022_Pb_ions_thick.seq', beam=True)


#%% Create a line for X-suite from MADX sequence 

madx.input('''
           SEQEDIT, SEQUENCE=PS;
           FLATTEN;
           REFER=CENTRE;
           FLATTEN;
           ''')

madx.use(sequence='ps')
# Slice and match tunes
n_slice_per_element = 15
seq_name = 'ps'
madx.input(f'''
select, flag=MAKETHIN, SLICE={n_slice_per_element}, thick=false;
MAKETHIN, SEQUENCE={seq_name}, MAKEDIPEDGE=true;
use, sequence={seq_name};

/******************************************************************
 *                   Tune matching with LEQ
 *
 *           Matching to the standard operational WP
 ******************************************************************/

Qx := 0.210;
Qy := 0.245;

use, sequence=PS;
exec, match_tunes_ptc(Qx, Qy);

/**********************************************************************************
* Low energy quadrupoles tune knobs
***********************************************************************************/

exec, tunes_leq_knob_factors(Qx, Qy);

''')



#"""
# Test the thin Twiss with MADX 
madx.use(sequence='ps')
twiss2 = madx.twiss()
fig = plt.figure(figsize=(10,7))
acc_lib.plot_tools.plot_twiss(fig, twiss2, twiss_from_madx=True)
fig.text(0.01, 0.93, 'MADX Twiss Thin', fontsize=22)
fig.savefig('PS_PB_Ion_flat_bottom_TwissThin_MADX.png', dpi=250)
#"""

madx.use(sequence='ps')
line = xt.Line.from_madx_sequence(madx.sequence[seq_name])
madx_beam = madx.sequence[seq_name].beam

particle_sample = xp.Particles(
        p0c = madx_beam.pc*1e9,
        q0 = madx_beam.charge,
        mass0 = madx_beam.mass*1e9)

line.particle_ref = particle_sample



#%% Perform Twiss command from tracker
tracker = xt.Tracker(_context=context, _buffer=buf, line=line) 
twiss_xtrack = tracker.twiss(method='4d')  # we only look at transverse coordinates
Qh_int, Qv_int = int(twiss_xtrack['qx']), int(twiss_xtrack['qy'])  # integer tune 
Qh, Qv = round(twiss_xtrack['qx'] % 1, 2), round(twiss_xtrack['qy'] % 1, 2)  # fractional working point, two decimal points
Qh_set = Qh_int + Qh
Qv_set = Qv_int + Qv

# Plot the Twiss 
fig2 = plt.figure(figsize=(10,7))
acc_lib.plot_tools.plot_twiss(fig2, twiss_xtrack, twiss_from_madx=False)
fig2.text(0.01, 0.93, 'Xtrack Twiss Thin', fontsize=22)
fig2.savefig('PS_PB_Ion_flat_bottom_TwissThin.png', dpi=250)

#%% Save sequences, either X-track or MAD-X 
if save_seq_to_json:
    with open('PS_sequence/PS_2022_Pb_ions.json', 'w') as fid:
        json.dump({
            'line': line.to_dict(),
            'particle': particle_sample.to_dict()},
            fid, cls=xo.JEncoder, indent=4)

if save_madx_seq:
    madx.command.save(sequence='ps', file='PS_sequence/PS_2022_Pb_ions_thin.seq', beam=True)

#####################################################
############ Sanity checks MADX vs Xsuite
#####################################################

# Step 1. Sanity checks from Gianni: Compare tunes, chromaticities and momentum compation factors
beta0 = line.particle_ref.beta0[0]
print("MAD-X:  " f"Qx  = {madx.table.summ['q1'][0]:.8f}"           f"   Qy  = {madx.table.summ['q2'][0]:.8f}")
print("Xsuite: " f"Qx  = {twiss_xtrack['qx']:.8f}"                        f"   Qy  = {twiss_xtrack['qy']:.8f}\n")
print("MAD-X:  " f"Q'x = {madx.table.summ['dq1'][0]*beta0:.7f}"    f"   Q'y = {madx.table.summ['dq2'][0]*beta0:.7f}")
print("Xsuite: " f"Q'x = {twiss_xtrack['dqx']:.7f}"                       f"   Q'y = {twiss_xtrack['dqy']:.7f}\n")
print("MAD-X:  " f"alpha_p = {madx.table.summ.alfa[0]:.7f}")
print("Xsuite: " f"alpha_p = {twiss_xtrack['momentum_compaction_factor']:.7f}")

print(madx_beam)


#%% Perform tracking
if test_xsuite_tracking:
    Qh_int, Qv_int = int(twiss_xtrack['qx']), int(twiss_xtrack['qy'])  # integer tune 
    Qh, Qy = round(twiss_xtrack['qx'] % 1, 2), round(twiss_xtrack['qy'] % 1, 2)  # fractional working point, two decimal points

    if x_suite_matched_Gaussian:
        # Generate matched gaussian distribution of particles and track them 
        # (we choose to match the distribution without accounting for spacecharge, just like in example
        particles_2 = xp.generate_matched_gaussian_bunch(_context=context,
                 num_particles=num_particles, total_intensity_particles=bunch_intensity,
                 nemitt_x=nemitt_x, nemitt_y=nemitt_y, sigma_z=sigma_z,
                particle_ref=particle_sample, tracker=tracker)
    else:
        # Define beam properties for distribution
        mad_beam = madx.sequence['sps'].beam
        x_offset = 0.0 #
        px_offset = 0.0
        x_dstb = np.random.normal(x_offset, 1e-6, num_particles)  #normal distribution in x
        px_dstb = np.random.normal(px_offset, 1e-6, num_particles)  #normal distribution in x
        y_dstb = np.random.normal(0, 1e-6, num_particles)  #normal distribution in x
        py_dstb = np.random.normal(0, 1e-6, num_particles)  #normal distribution in x
        zeta_dstb = np.random.normal(0, 1e-8, num_particles) 
        delta_dstb = np.random.normal(0, 1e-6, num_particles) 

        
        particles_2 = xp.Particles(
                mass0=mad_beam.mass*1e9, gamma0=mad_beam.gamma, q0 = mad_beam.charge, p0c = mad_beam.pc*1e9,
                x=x_dstb, px=px_dstb, y=y_dstb, py=py_dstb,
                zeta=zeta_dstb, delta=delta_dstb)

    # Track the particles
    tracker.track(particles_2, num_turns=num_turns, turn_by_turn_monitor=True)
    
    
    # Plot the results
    fig = plt.figure(figsize=(10,7))
    Q_x,  Q_y, ax1 = acc_lib.plot_tools.get_tune_footprint(fig, tracker)
    ax1.plot(Qh, Qy, marker='*', markersize=15, label='Fractional working point')
    fig.suptitle('Tune footprint {}, {} particles for {} turns'.format(fig_str, num_particles, num_turns),fontsize=17)
    fig.tight_layout()
    ax1.legend()
    
    # Plot tune footprint with resonances from working point
    fig1 = plt.figure(figsize=(10,7))
    Q_interval = 1e-1
    tune_diagram = acc_lib.resonance_lines([Qh_set-Q_interval,Qh_set+Q_interval],[Qv_set-Q_interval,Qv_set+Q_interval], 1+np.arange(4), 16)
    ax11, Q_x, Q_y= tune_diagram.plot_resonance_and_tune_footprint(tracker, Qh_int, fig1)
    
    # Plot turn-by-turn centroid 
    fig2 = plt.figure(figsize=(10,7))
    acc_lib.plot_tools.plot_centroid_motion(fig2, tracker)

    # Plot the phase space diagram of the first particle
    fig3 = plt.figure(figsize=(10,7))
    acc_lib.plot_tools.plot_phase_space_ellipse(fig3, tracker)
    